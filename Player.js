class Player extends Phaser.Physics.Arcade.Sprite {
  constructor(scene, x, y) {
    super(scene, x, y, "dude");
    this.scene = scene;
    scene.physics.world.enable(this);
    scene.add.existing(this);
    this.setCollideWorldBounds(true);
    this.left=false;
    this.right=false;
    this.jump=false;
    this.score=0;
  }
  
  getScore(){
    return this.score;
  }
  
  setScore(s){
    this.score=s;
  }
  
  goLeft(){
    this.left=true;
  }
  
  goRight(){
    this.right=true;
  }
  
  goJump(){
    this.jump=true;
  }
  
  idle(){
    this.left=false;
    this.right=false;
    this.jump=false;
  }
  
  update(){
    
  }
}