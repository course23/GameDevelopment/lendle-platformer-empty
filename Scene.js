/*
 * 定義一個 scene，用成員變數儲存 scene 上面的物件
 * override preload, create, update 函式
 */
class Scene extends Phaser.Scene {
  /*********************************************************************************************************************** */
  constructor() {
    super();
  }

  /*********************************************************************************************************************** */
  preload() {
    this.load.image(
      "sky",
      "https://cdn.glitch.com/9a89ca9c-b3e2-426b-b4c6-6e332ff1a0e4%2Fsky.png?v=1583115110427"
    );
    this.load.image(
      "ground",
      "https://cdn.glitch.com/9a89ca9c-b3e2-426b-b4c6-6e332ff1a0e4%2Fplatform.png?v=1583115124057"
    );
    this.load.image(
      "star",
      "https://cdn.glitch.com/9a89ca9c-b3e2-426b-b4c6-6e332ff1a0e4%2Fstar.png?v=1583115116442"
    );
    this.load.image(
      "bomb",
      "https://cdn.glitch.com/9a89ca9c-b3e2-426b-b4c6-6e332ff1a0e4%2Fbomb.png?v=1583115128502"
    );
    this.load.spritesheet(
      "dude",
      "https://cdn.glitch.com/9a89ca9c-b3e2-426b-b4c6-6e332ff1a0e4%2Fdude.png?v=1583115132173",
      {frameWidth: 32, frameHeight: 48}
    );
  }

  /*********************************************************************************************************************** */
  create() {}

  /*********************************************************************************************************************** */
  update() {}
}
